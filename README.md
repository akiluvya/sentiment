# README #

This my sentiment analysis application. 

So far I have implemented the following:
=> Twitter Streaming API client

=> Train Algorithm for classifier(Naive Bayse, 70-30)

=> Predict algorithm based on trained model

=> I added stemming and lemmatization



Libraries I have used

=>spark-mllib

=>stanford-corenlp

=>twitter4j

=>spark-streaming-twitter

=>other libs are common spark, hadoop and scala libs

It achieves Accuracy of 0.7319071441851032

What is left to do:

=> push the features to the database, I am thinking of using mongoBD or any other nosql database;

=>Visualize my findings in tableau or bokeh.