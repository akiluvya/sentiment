package com.kiluvya

import org.apache.commons.cli.{Options, ParseException, PosixParser}
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.feature.HashingTF
import twitter4j.auth.OAuthAuthorization
import twitter4j.conf.ConfigurationBuilder
import scala.util.matching.Regex;
import java.io.InputStream
import java.util.Properties

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext

import edu.stanford.nlp.ling.CoreAnnotations.{LemmaAnnotation, SentencesAnnotation, TokensAnnotation}
import edu.stanford.nlp.pipeline.{Annotation, StanfordCoreNLP}

import scala.collection.JavaConverters._
import scala.collection.mutable.ArrayBuffer

object Utils{

  val numFeatures = 50000
  val tf = new HashingTF(numFeatures)
  val pipline = createNLPPipeline();
  val CONSUMER_KEY = "gfqIAJKwKzgFNQb9QOsnVqRa1"
  val CONSUMER_SECRET = "rN1LGfnVHYnrOn5i1aXjpVaGtFmW2OWrMcKQsZlNBuidbGA9hq"
  val ACCESS_TOKEN = "3324934254-xRJF2rOUPPLZkX4FNeyRpO75SRRbmve2dql47dS"
  val ACCESS_TOKEN_SECRET = "9ICrg59Zy8KGYa9ENUG1zq8ks9PmXfZJEsBtNwgOFM6MC"
  
  val pattern = new Regex("![a-z]");

  val THE_OPTIONS = {
    val options = new Options()
    options
  }

  def parseCommandLineWithTwitterCredentials(args: Array[String]) = {
    val parser = new PosixParser
    try {
      val cl = parser.parse(THE_OPTIONS, args)
      System.setProperty("twitter4j.oauth.consumerKey", CONSUMER_KEY)
      System.setProperty("twitter4j.oauth.consumerSecret", CONSUMER_SECRET)
      System.setProperty("twitter4j.oauth.accessToken", ACCESS_TOKEN)
      System.setProperty("twitter4j.oauth.accessTokenSecret", ACCESS_TOKEN_SECRET)
      cl.getArgList.toArray
    } catch {
      case e: ParseException =>
        System.err.println("Parsing failed.  Reason: " + e.getMessage)
        System.exit(1)
    }
  }

  def getAuth = {
    Some(new OAuthAuthorization(new ConfigurationBuilder().build()))
  }

  /**
   * Create feature vectors by turning each tweet into bigrams of characters (an n-gram model)
   * and then hashing those to a length-1000 feature vector that we can pass to MLlib.
   * This is a common way to decrease the number of features in a model while still
   * getting excellent accuracy (otherwise every pair of Unicode characters would
   * potentially be a feature).
   */
  def featurize(s: String, stops: Set[String]): Vector = {
	//System.out.println(s.sliding(2).toSeq)
    //tf.transform(s.sliding(2).toSeq)
	//val ordinary=(('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')).toSet
	//def isOrdinary(s:String)=s.forall(ordinary.contains(_))
	//val sq = s.toLowerCase.split(' ').filter(isOrdinary(_)).toSeq;
	//System.out.println(sq)
	//tf.transform(s.toLowerCase.split(' ').filter(isOrdinary(_)).
	//filter(_.length != 0).toSeq)
	var seq = plainTextToLemmas(s.toLowerCase, stops, pipline).toSeq;
	tf.transform(seq)
  }

  object IntParam {
    def unapply(str: String): Option[Int] = {
      try {
        Some(str.toInt)
      } catch {
        case e: NumberFormatException => None
      }
    }
  }

  def createNLPPipeline(): StanfordCoreNLP = {
    val props = new Properties()
    props.put("annotators", "tokenize, ssplit, pos, lemma")
    new StanfordCoreNLP(props)
  }

  def plainTextToLemmas(text: String, stopWords: Set[String], pipeline: StanfordCoreNLP)
  : Seq[String] = {
    val doc = new Annotation(text)
    pipeline.annotate(doc)
    val lemmas = new ArrayBuffer[String]()
    val sentences = doc.get(classOf[SentencesAnnotation])
    for (sentence <- sentences.asScala;
         token <- sentence.get(classOf[TokensAnnotation]).asScala) {
      val lemma = token.get(classOf[LemmaAnnotation])
      if (lemma.length > 2 && !stopWords.contains(lemma) && isOnlyLetters(lemma)) {
        lemmas += lemma.toLowerCase
      }
    }
    lemmas
  }

  def isOnlyLetters(str: String): Boolean = {
    var i = 0
    while (i < str.length) {
      if (!Character.isLetter(str.charAt(i))) {
        return false
      }
      i += 1
    }
    true
  }

   def loadStopWords(path: String) = {
    val stream: InputStream = getClass.getResourceAsStream(path)
    val lines = scala.io.Source.fromInputStream(stream).getLines
    lines.toSet
  }


}
