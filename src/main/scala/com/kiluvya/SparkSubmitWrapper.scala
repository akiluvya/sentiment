package com.kiluvya

import org.apache.spark.deploy.SparkSubmit

object SparkSubmitWrapper {
  def main(args: Array[String]): Unit = {
    SparkSubmit.main(args)
  }
}
